package nortti.ru.qrstor.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {


    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "mainDb";
    public static final String TABLE_STORAGES = "storages";
    public static final String TABLE_PRODUCTS = "products";
    public static final String TABLE_STATUSES = "statuses";
    public static final String TABLE_INSPECTORS = "inspectors";

    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_PHONE = "phone";

    public static final String KEY_STORAGE_ID = "storage_id";
    public static final String KEY_STATUS_ID = "status_id";
    public static final String KEY_INSPECTOR_ID = "inspector_id";
    public static final String KEY_SUPPLIER_ID = "supplier_id";
    public static final String KEY_BARCODE = "barcode";
    public static final String KEY_COUNT = "count";
    public static final String KEY_DATE = "date";
    public static final String KEY_ROLE = "role";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_STORAGES + "(" + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " text," + KEY_ADDRESS + " text,"+ KEY_PHONE + " text" + ")");
        db.execSQL("create table " + TABLE_PRODUCTS + "(" + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " text," + KEY_BARCODE + " text,"+ KEY_COUNT + " text," + KEY_DATE + " text,"+ KEY_STATUS_ID + " integer,"+ KEY_STORAGE_ID + " integer" + ")");
        db.execSQL("create table " + TABLE_STATUSES + "(" + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " text" + ")");
        db.execSQL("create table " + TABLE_INSPECTORS + "(" + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " text,"+ KEY_ROLE + " text" + ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_STORAGES);
        db.execSQL("drop table if exists " + TABLE_PRODUCTS);
        db.execSQL("drop table if exists " + TABLE_STATUSES);
        db.execSQL("drop table if exists " + TABLE_INSPECTORS);


        onCreate(db);

    }
}
