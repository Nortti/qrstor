package nortti.ru.qrstor.activities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

import nortti.ru.qrstor.R;
import nortti.ru.qrstor.database.DBHelper;

public class AddProductActivity extends AppCompatActivity implements OnClickListener{

    EditText nameEd, barcodeEd, dateEd, countEd;
    Spinner statusSP, storageSP;
    Button addButton;
    DBHelper DBHelper;
    ArrayList<String> storages;
    SQLiteDatabase dbStor, dbProd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        nameEd = (EditText) findViewById(R.id.name);
        barcodeEd = (EditText) findViewById(R.id.barcode);
        dateEd = (EditText) findViewById(R.id.date);
        countEd = (EditText) findViewById(R.id.count);

        statusSP = (Spinner) findViewById(R.id.status);
        storageSP = (Spinner) findViewById(R.id.storage);

        addButton = (Button) findViewById(R.id.addBut);
        addButton.setOnClickListener(this);

        DBHelper = new DBHelper(this);

        dbStor = DBHelper.getWritableDatabase();

        Cursor cursor = dbStor.query(DBHelper.TABLE_STORAGES, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(DBHelper.KEY_NAME);
            storages = new ArrayList<>();
            storages.add(cursor.getString(nameIndex));
        }



        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.status));
        statusSP.setAdapter(dataAdapter);

        ArrayAdapter<String> storageAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, storages);
        storageSP.setAdapter(storageAdapter);


    }

    @Override
    public void onClick(View v) {

        String name = nameEd.getText().toString();
        String barcode = barcodeEd.getText().toString();
        String date = dateEd.getText().toString();
        String count = countEd.getText().toString();
        String status = statusSP.getSelectedItem().toString();

        ContentValues contentValues = new ContentValues();

        switch (v.getId()){
            case R.id.addBut:

                break;
        }
    }
}
