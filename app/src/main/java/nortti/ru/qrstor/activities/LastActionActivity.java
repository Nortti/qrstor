package nortti.ru.qrstor.activities;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.joaquimley.faboptions.FabOptions;

import java.util.ArrayList;

import nortti.ru.qrstor.R;
import nortti.ru.qrstor.adapters.ActionAdapter;
import nortti.ru.qrstor.database.DBHelper;
import nortti.ru.qrstor.models.Action;

public class LastActionActivity extends AppCompatActivity implements OnClickListener {

    ListView lsView;
    TextView no_items, lastAc, textSwitcher;
    EditText nameStor, addressStor, phoneStor;
    Button add_stor;
    FabOptions fabOptions;
    View actionView;
    ArrayList<Action> actions;
    Action action;
    ActionAdapter actionAdapter;
    String[] titles, barcodes, counts, dates, statuses, storageList;
    boolean isSelected;

    DBHelper DBHelper;

    String name, address, phone;

    SQLiteDatabase databaseStor;
    ContentValues contentValues;

    private Toolbar toolbar;

    private Spinner spinner_nav;
    private ArrayList<String> category=null;

    SimpleCursorAdapter spinAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_action);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        spinner_nav = (Spinner) findViewById(R.id.spinner_nav);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }

        category = new ArrayList<>();



        actions = new ArrayList<>();

     /*   textSwitcher = (TextView) findViewById(R.id.textSwitch);
        textSwitcher.setOnClickListener(this);*/

        actionView = findViewById(R.id.actionView);


        lsView = (ListView) actionView.findViewById(R.id.lsAction);
        lastAc = (TextView) actionView.findViewById(R.id.lastAc);
        no_items = (TextView) actionView.findViewById(R.id.no_items);

        add_stor = (Button) actionView.findViewById(R.id.add_stor);
        add_stor.setOnClickListener(this);


        fabOptions = (FabOptions) findViewById(R.id.fab_options);
        fabOptions.setOnClickListener(this);

        DBHelper = new DBHelper(this);

        if (!isSelected){
            lsView.setVisibility(View.GONE);
            lastAc.setVisibility(View.GONE);
            fabOptions.setVisibility(View.INVISIBLE);
            findViewById(R.id.no_items_layout).setVisibility(View.VISIBLE);
        }

        updateStorageList();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // получим идентификатор выбранного пункта меню
        int id = item.getItemId();

        switch (id) {
            case R.id.add_storage:
                addStorage();
                return true;
            case R.id.add_inspector:
                addInspector();
                return true;
            case R.id.add_supplier:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {


        databaseStor = DBHelper.getWritableDatabase();

        contentValues = new ContentValues();



        switch (v.getId()){

            case R.id.add_stor:
                addStorage();
                break;
            case R.id.faboptions_search:
                startActivity(new Intent(this,AddProductActivity.class));
                break;
            case R.id.faboptions_barcode:
                Toast.makeText(this, getString(R.string.barcode), Toast.LENGTH_SHORT).show();
                break;
            case R.id.faboptions_call:
                Toast.makeText(this, getString(R.string.call), Toast.LENGTH_SHORT).show();
                break;
            case R.id.faboptions_settings:
                Toast.makeText(this, getString(R.string.settings), Toast.LENGTH_SHORT).show();
                break;

        }
    }

    private void updateStorageList(){
        DBHelper = new DBHelper(LastActionActivity.this);
        SQLiteDatabase sqLiteDatabase = DBHelper.getReadableDatabase();

        Cursor cursor = sqLiteDatabase.query(DBHelper.TABLE_STORAGES, new String[]{DBHelper.KEY_ID, DBHelper.KEY_NAME}, null, null, null, null, null);

        spinAdapter = new SimpleCursorAdapter(
                this,
                R.layout.spinner_row,
                cursor,
                new String[]{DBHelper.KEY_NAME},
                new int[]{R.id.tvCategory},
                0
        );

        if (spinAdapter.isEmpty()){
            spinner_nav.setVisibility(View.INVISIBLE);
        } else {
            spinner_nav.setVisibility(View.VISIBLE);
            spinner_nav.setAdapter(spinAdapter);
            findViewById(R.id.no_items_layout).setVisibility(View.INVISIBLE);
            fabOptions.setVisibility(View.VISIBLE);

        }
    }

    private void updateInspectorList(){

    }

    private void addStorage(){
        AlertDialog.Builder storageBuilder = new AlertDialog.Builder(this);
        storageBuilder.setTitle(getString(R.string.new_stor_tit));
        storageBuilder.setMessage(getString(R.string.new_stor_descr));
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText nameBox = new EditText(this);
        nameBox.setHint(getString(R.string.name));
        layout.addView(nameBox);

        final EditText addressBox = new EditText(this);
        addressBox.setHint(getString(R.string.address));
        layout.addView(addressBox);

        final EditText phoneBox = new EditText(this);
        phoneBox.setHint(getString(R.string.phone));
        phoneBox.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        phoneBox.setInputType(InputType.TYPE_CLASS_PHONE);
        layout.addView(phoneBox);
        storageBuilder.setView(layout);

        storageBuilder.setPositiveButton(getString(R.string.add), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String nameInput = nameBox.getText().toString();
                String addressInput = addressBox.getText().toString();
                String phoneInput = phoneBox.getText().toString();
                DBHelper = new DBHelper(LastActionActivity.this);

                SQLiteDatabase sqLiteDatabase = DBHelper.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.clear();

                contentValues.put(DBHelper.KEY_NAME, nameInput);
                contentValues.put(DBHelper.KEY_ADDRESS, addressInput);
                contentValues.put(DBHelper.KEY_PHONE, phoneInput);
                sqLiteDatabase.insertWithOnConflict(DBHelper.TABLE_STORAGES,null,contentValues,SQLiteDatabase.CONFLICT_IGNORE);
                updateStorageList();
            }
        });

        storageBuilder.setNegativeButton(getString(R.string.cancel), null);

        storageBuilder.create().show();
        fabOptions.setVisibility(View.INVISIBLE);
    }

    private void addInspector(){
        AlertDialog.Builder inspectorBuilder = new AlertDialog.Builder(this);
        inspectorBuilder.setTitle(getString(R.string.new_insp_tit));
        inspectorBuilder.setMessage(getString(R.string.new_insp_descr));
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText nameBox = new EditText(this);
        nameBox.setHint(getString(R.string.ins_name));
        layout.addView(nameBox);

        final EditText roleBox = new EditText(this);
        roleBox.setHint(getString(R.string.ins_role));
        layout.addView(roleBox);

        inspectorBuilder.setView(layout);

        inspectorBuilder.setPositiveButton(getString(R.string.add), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String nameInput = nameBox.getText().toString();
                String roleInput = roleBox.getText().toString();
                DBHelper = new DBHelper(LastActionActivity.this);

                SQLiteDatabase sqLiteDatabase = DBHelper.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.clear();

                contentValues.put(DBHelper.KEY_NAME, nameInput);
                contentValues.put(DBHelper.KEY_ROLE, roleInput);
                sqLiteDatabase.insertWithOnConflict(DBHelper.TABLE_INSPECTORS,null,contentValues,SQLiteDatabase.CONFLICT_IGNORE);
            }
        });

        inspectorBuilder.setNegativeButton(getString(R.string.cancel), null);

        inspectorBuilder.create().show();
    }


}
