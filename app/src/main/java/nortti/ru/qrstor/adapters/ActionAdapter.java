package nortti.ru.qrstor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import nortti.ru.qrstor.R;
import nortti.ru.qrstor.models.Action;

/**
 * Created by nortti on 11.04.17.
 */

public class ActionAdapter extends BaseAdapter {

    ArrayList<Action> actions = new ArrayList<>();


    Context mContext;

    public ActionAdapter(Context mContext, ArrayList<Action> actions) {
        this.mContext = mContext;
        this.actions = actions;
    }

    @Override
    public int getCount() {
        return actions.size();
    }

    @Override
    public Action getItem(int position) {
        return actions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View list;

        if (convertView == null) {
            list = new View(mContext);
            //LayoutInflater inflater = getLayoutInflater();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            list = inflater.inflate(R.layout.action_item, parent, false);
        } else {
            list = (View) convertView;
        }

        TextView titleView = (TextView) list.findViewById(R.id.title);
        titleView.setText(actions.get(position).getTitle());

        TextView barcodeView = (TextView) list.findViewById(R.id.barcode);
        barcodeView.setText(actions.get(position).getBarcode());

        TextView countView = (TextView) list.findViewById(R.id.count);
        countView.setText(actions.get(position).getCount());

        TextView dateView = (TextView) list.findViewById(R.id.date);
        dateView.setText(actions.get(position).getDate());

        TextView statusVIew = (TextView) list.findViewById(R.id.status);
        statusVIew.setText(actions.get(position).getStatus());


        return list;
    }
}
