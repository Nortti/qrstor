package nortti.ru.qrstor.models;

/**
 * Created by nortti on 11.04.17.
 */

public class Action {
    String title, barcode, count, date, status, inspector, supplier, storage;

    public Action(String title, String barcode, String count, String date, String status) {
        this.title = title;
        this.barcode = barcode;
        this.count = count;
        this.date = date;
        this.status = status;
    }

    public Action(String title, String barcode, String count, String date, String status, String inspector, String supplier, String storage) {
        this.title = title;
        this.barcode = barcode;
        this.count = count;
        this.date = date;
        this.status = status;
        this.inspector = inspector;
        this.supplier = supplier;
        this.storage = storage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }
}
